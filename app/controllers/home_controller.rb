require 'json'
require 'faraday'

class HomeController < ApplicationController
  def index

    t = Faraday.get 'https://abc-project-4efac.firebaseio.com/University/Koreatech/Module/KUT-0-0/20170819184846_KUT-0-0_kildeok.json'
    pp = JSON.parse(t.body).to_a.reverse[0][1].to_h

    # date = Time.now.strftime("%Y%m%d%H%M%S")
    # model_id = "KUT-0-0"
    # user_id = "kildeok"

    @location = []
    @location << pp["Latitude"]
    @location << pp["Longitude"]
  end

end
